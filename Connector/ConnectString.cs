﻿using System.Configuration;

namespace Connector.DatabaseConfig
{
    public static class ConnectStringManager
    {
        public static void SetConnectionString(string connectString)
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var connectionStringsSection = (ConnectionStringsSection)config.GetSection("connectionStrings");
            connectionStringsSection.ConnectionStrings["EmployesEntities"].ConnectionString = connectString;
            config.Save();
            ConfigurationManager.RefreshSection("connectionStrings");
        }

        public static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["EmployesEntities"].ConnectionString;
        }
    }
}
