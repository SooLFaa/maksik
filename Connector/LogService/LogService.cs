﻿using System.Collections.Generic;
using System.Linq;
using Connector.Entity;
using Connector.Types;
using System.IO;
using System;

namespace Connector.Services
{
    public class LogService : ILogService
    {
        public void Log(Users user)
        {
            using (StreamWriter sw = File.AppendText("log.txt"))
            {
                sw.WriteLine(String.Format("Дата: {0} Логин пользователя: {1}", DateTime.Now.ToString("dd:MM:yyyy HH:mm"), user.Login));
            }
        }
    }
}
