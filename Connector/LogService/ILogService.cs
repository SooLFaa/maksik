﻿using Connector.Entity;
using Connector.Types;
using System;
using System.Collections.Generic;
using System.IO;

namespace Connector.Services
{
    public interface ILogService
    {
        void Log(Users user);
    }
}
