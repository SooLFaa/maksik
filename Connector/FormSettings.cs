﻿using System;
using System.Windows.Forms;
using Connector.DatabaseConfig;

namespace Connector
{
    public partial class FormSettings : Form
    {
        public FormSettings()
        {
            InitializeComponent();
        }

        private void FormSettings_Load(object sender, EventArgs e)
        {
            textBoxConnString.Text = ConnectStringManager.GetConnectionString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            ConnectStringManager.SetConnectionString(textBoxConnString.Text);
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
