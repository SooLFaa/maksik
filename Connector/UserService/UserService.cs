﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connector.Entity;
using Connector.Services;

namespace Connector.Services
{
    public class UserService : IUserService
    {
        public Users GetUserByCredentional(string userName, string password)
        {
            EmployesEntities context = new EmployesEntities();
            return context.Users.Where(x => x.Login.Equals(userName) && x.Password.Equals(password)).FirstOrDefault();
        }
    }
}
