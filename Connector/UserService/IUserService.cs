﻿using Connector.Entity;

namespace Connector.Services
{
    public interface IUserService
    {
        Users GetUserByCredentional(string userName, string password);
    }
}
