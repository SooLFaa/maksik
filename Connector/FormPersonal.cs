﻿using Connector.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Connector
{
    public partial class FormPersonal : Form
    {
        public FormPersonal()
        {
            InitializeComponent();
        }

        public FormPersonal(Personal personal) : this()
        {
            Personal = personal;
            textBoxFio.Text = Personal.Fio;
            textBoxMobile.Text = Personal.Phone;
            textBoxAdress.Text = Personal.Adress;
            textBoxDescription.Text = Personal.Description;
            textBoxHomePhone.Text = Personal.PhoneHome;
        }

        public Personal Personal { get; private set; }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            Personal = new Personal();
            Personal.Fio = textBoxFio.Text;
            Personal.Phone = textBoxMobile.Text;
            Personal.Adress = textBoxAdress.Text;
            Personal.Description = textBoxDescription.Text;
            Personal.PhoneHome = textBoxHomePhone.Text;

            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
