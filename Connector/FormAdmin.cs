﻿using Connector.Services;
using System;
using System.Windows.Forms;
using Connector.Entity;
using System.Linq;
using System.Data;

namespace Connector
{
    public partial class FormAdmin : Form
    {
        IPersonalService _personalService = new PersonalService();
        public FormAdmin(Users user)
        {
            InitializeComponent();
            
            if (user.IsAdmin.Value)
            {
                Text = "Админ";
                toolStrip1.Enabled = true;
            }
            else
            {
                Text = "Пользователь";
                toolStrip1.Enabled = false;
            }

            dataGridViewPersonal.DataSource = _personalService.GetPersonals();
        }

        private void FormAdmin_Load(object sender, EventArgs e)
        {
            dataGridViewPersonal.DataSource = _personalService.GetPersonalByFind();
        }

        private void buttonFind_Click(object sender, EventArgs e)
        {
            dataGridViewPersonal.DataSource = _personalService.GetPersonalByFind(textBoxFio.Text, textBoxAdress.Text, textBoxMobile.Text, textBoxHomePhone.Text);
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
             FormPersonal form = new FormPersonal();
             if (form.ShowDialog() != DialogResult.OK)
             {
                 return;
             }
             _personalService.AddPersonal(form.Personal);
             dataGridViewPersonal.DataSource = _personalService.GetPersonals();
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            DataGridViewCell cell = dataGridViewPersonal[0, dataGridViewPersonal.SelectedCells[0].RowIndex];
            int id = int.Parse(cell.Value.ToString());

            _personalService.RemovePersonal(id);
            dataGridViewPersonal.DataSource = _personalService.GetPersonals();
        }

        private void dataGridViewPersonal_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            switch (e.ColumnIndex)
            {
                case 0:
                    dataGridViewPersonal.DataSource = _personalService.GetPersonals();
                    break;
                case 1:
                    dataGridViewPersonal.DataSource = _personalService.GetPersonals(Types.OrderEnum.Fio);
                    break;
                case 2:
                    dataGridViewPersonal.DataSource = _personalService.GetPersonals(Types.OrderEnum.Adress);
                    break;
                case 3:
                    dataGridViewPersonal.DataSource = _personalService.GetPersonals(Types.OrderEnum.PhoneMobile);
                    break;
                case 4:
                    dataGridViewPersonal.DataSource = _personalService.GetPersonals(Types.OrderEnum.PhoneHome);
                    break;
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            DataGridViewCell cell = dataGridViewPersonal[0, dataGridViewPersonal.SelectedCells[0].RowIndex];
            int id = int.Parse(cell.Value.ToString());
            Personal model = _personalService.GetPersonalById(id);

            FormPersonal form = new FormPersonal(model);
            if (form.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            _personalService.EditPersonal(id, form.Personal);
            dataGridViewPersonal.DataSource = _personalService.GetPersonals();
        }

        private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }

        private void копироватьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int rowIndex = dataGridViewPersonal.SelectedCells[0].RowIndex;
            DataGridViewCellCollection cells = dataGridViewPersonal.Rows[rowIndex].Cells;
            Clipboard.SetText(string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}", cells[0].Value, cells[1].Value, cells[2].Value, cells[3].Value, cells[4].Value, cells[5].Value));
        }
    }
}
