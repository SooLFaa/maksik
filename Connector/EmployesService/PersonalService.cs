﻿using System.Collections.Generic;
using System.Linq;
using Connector.Entity;
using Connector.Types;

namespace Connector.Services
{
    public class PersonalService : IPersonalService
    {
        private EmployesEntities context = new EmployesEntities();

        public List<Personal> GetPersonalByFind(string fio = "", string adress = "", string mobilePhone = "", string homePhone = "")
        {
            return context.Personal.Where(x =>
                (string.IsNullOrEmpty(fio) || x.Fio.ToLower().Contains(fio.ToLower())) 
             && (string.IsNullOrEmpty(adress) || x.Adress.ToLower().Contains(adress.ToLower()))
             && (string.IsNullOrEmpty(mobilePhone) || x.Phone.ToLower().Contains(mobilePhone))
             && (string.IsNullOrEmpty(homePhone) || x.PhoneHome.ToLower().Contains(homePhone))
            ).ToList();
        }
        public List<Personal> GetPersonals(OrderEnum orderColumn = OrderEnum.Number)
        {
            List<Personal> personal = new List<Personal>();
            switch(orderColumn) {
                case OrderEnum.Fio:
                    personal = context.Personal.OrderBy(x => x.Fio).ToList();
                    break;
                case OrderEnum.Number:
                    personal = context.Personal.OrderBy(x => x.ID).ToList();
                    break;
                case OrderEnum.PhoneHome:
                    personal = context.Personal.OrderBy(x => x.PhoneHome).ToList();
                    break;
                case OrderEnum.PhoneMobile:
                    personal = context.Personal.OrderBy(x => x.Phone).ToList();
                    break;
                case OrderEnum.Adress:
                    personal = context.Personal.OrderBy(x => x.Adress).ToList();
                    break;
            }

            return personal;
        }

        public Personal GetPersonalById(int id)
        {
            return context.Personal.Where(x => x.ID == id).FirstOrDefault();
        }

        public void AddPersonal(Personal personal)
        {
            context.Personal.Add(personal);
            context.SaveChanges();
        }

        public void RemovePersonal(int personalId)
        {
            context.Personal.Remove(context.Personal.Where(x => x.ID == personalId).FirstOrDefault());
            context.SaveChanges();
        }

        public void EditPersonal(int id, Personal personal)
        {
            Personal persOld = context.Personal.Where(x => x.ID == id).FirstOrDefault();
            persOld.Fio = personal.Fio;
            persOld.Adress = personal.Adress;
            persOld.Description = personal.Description;
            persOld.Phone = personal.Phone;
            persOld.PhoneHome = personal.PhoneHome;
            context.SaveChanges();
        }
    }
}
