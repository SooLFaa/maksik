﻿using Connector.Entity;
using Connector.Types;
using System.Collections.Generic;

namespace Connector.Services
{
    public interface IPersonalService
    {
        List<Personal> GetPersonalByFind(string fio = "", string adress = "", string mobilePhone = "", string homePhone = "");
        List<Personal> GetPersonals(OrderEnum orderColumn = OrderEnum.Number);
        Personal GetPersonalById(int id);
        void AddPersonal(Personal personal);
        void RemovePersonal(int personalId);
        void EditPersonal(int id, Personal personal);
    }
}
