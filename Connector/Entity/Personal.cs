//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Connector.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class Personal
    {
        public int ID { get; set; }
        public string Fio { get; set; }
        public string Adress { get; set; }
        public string Phone { get; set; }
        public string PhoneHome { get; set; }
        public string Description { get; set; }
    }
}
