﻿namespace Connector.Types
{
    public enum OrderEnum
    {
        Number = 0,
        Fio = 1, 
        Adress = 2, 
        PhoneMobile = 3,
        PhoneHome = 4
    }
}
