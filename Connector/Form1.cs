﻿using System;
using System.Windows.Forms;
using Connector.Entity;
using Connector.Services;

namespace Connector
{
    public partial class FormLogin : Form
    {
        public IUserService _userService;
        public ILogService _logService;

        public FormLogin()
        {
            InitializeComponent();
            _userService = new UserService();
            _logService = new LogService();
        }

        private void buttonChangeDatabase_Click(object sender, EventArgs e)
        {
            FormSettings form = new FormSettings();
            if (form.ShowDialog() != DialogResult.OK) 
            {
                return;
            }
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            Users user = _userService.GetUserByCredentional(textBoxLogin.Text, textBoxPassword.Text);

            if (user == null)
            {
                MessageBox.Show("Не верный логин или пароль");
                return;
            }

            _logService.Log(user);
            FormAdmin form = new FormAdmin(user);
            form.Show();

            Hide(); 


        }
    }
}
